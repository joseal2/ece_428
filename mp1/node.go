package main

import (
	"bufio"
	"container/heap"
	"encoding/gob"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

/*
Need message struct -> types are DEPOSIT, TRANSFER
Create map that holds accounts, create account when new is seen
Print balance after every transaction
create priority queue for handling messages
handle failures
all nodes process the same transactions in the same order

*/
const ARG_NUM_NODE int = 3

var conn_in net.Conn
var id_slice []string   //keep track of id's
var port_slice []string //keep track of ports
var addr_slice []string //keep track of addresses
var proc_slice []int    //count requested priorities received
var msg_slice []Message //store the messages we have sent out

var my_id string
var connected_nodes int
var in_map map[string]net.Conn      //keep track of incoming connections
var out_map map[string]*gob.Encoder //keep track of outgoing connections
var msg_map map[Pair]*Item          //keep track of items in PQ for update()
var bal_map map[string]int          //keep track of all accounts and balances

var mu_in sync.Mutex
var mu_out sync.Mutex
var mu_p sync.Mutex           //mutex for priority variable
var mu_bal sync.Mutex         //mutex for balance map
var mu_proc sync.Mutex        //mutex for process response counters
var mu_msg sync.Mutex         //mutex for message slice
var mu_msg_map sync.Mutex     //mutex for message map
var mu_pq sync.Mutex          //mutex for PQ
var mu_id sync.Mutex          //mutex for id slice
var max_priority_seen int = 0 //highest priority requested/delivered
var cur_process int = 1       //current process that we are handling, increment on send
var mu_w sync.Mutex           //writer mutex
var W_t, W_b *bufio.Writer

var pq PriorityQueue

type Message struct {
	Gen           int64
	Id            string
	Msg_type, Amt int
	Acct1, Acct2  string
	Priority      int
	Process_num   int
}

type Item struct {
	msg   Message
	Index int
	valid bool
	start time.Time
}

type Pair struct {
	Id      string
	Process int
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	if pq[i].msg.Priority == pq[j].msg.Priority {
		return pq[i].msg.Process_num < pq[j].msg.Process_num
	}
	return pq[i].msg.Priority < pq[j].msg.Priority
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	item.Index = -1
	*pq = old[0 : n-1]
	return item
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Item)
	item.Index = n
	*pq = append(*pq, item)
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].Index = i
	pq[j].Index = j
}

// update modifies the priority and value of an Item in the queue.
func (pq *PriorityQueue) update(msg Message, valid bool, priority, t int) {
	p := Pair{Process: msg.Process_num, Id: msg.Id} //removed priority
	item := msg_map[p]
	if item == nil {
		fmt.Printf("failed trying to update %v\n", msg)
		fmt.Println(msg_map)
		fmt.Println(p)
	}
	item.valid = valid
	item.msg.Priority = priority
	item.msg.Msg_type = t
	heap.Fix(pq, item.Index)
}

func handle_err(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}

// type Message struct {
// 	id           string
// 	msg_type,amt int
// 	acct1, acct2 string
// 	priority     int
// }
//0 first msg, 1 deposit request, 2 deposit response, 3 final deposit
//4 trans request, 5 trans response, 6 final trans
func handle_receive(conn net.Conn) {
	//fmt.Printf("hello\n")
	msg := &Message{}
	dec := gob.NewDecoder(conn)
	dec.Decode(msg)
	//fmt.Printf("node id is: %s \n", msg.Id)
	//store_id := msg.Id
	mu_in.Lock()
	in_map[msg.Id] = conn
	mu_in.Unlock()
	//node_id = msg.id[:len(node_id)-1]
	//timeinmicros := time.Now().UnixNano() / 100
	//timeinsecs := float64(timeinmicros) / 1000000
	//fmt.Fprintf(os.Stdout, "%.6f - %s connected\n", timeinsecs, msg.Id)
	//fmt.Printf("Now receiving from: %s\n", msg.Id)

	for {
		//fmt.Printf("Trying to get msg\n")
		err := dec.Decode(msg)
		if err != nil {
			//fmt.Printf("%s disconnected\n", msg.Id)
			mu_id.Lock()
			for i, name := range id_slice {
				//remove the dead node
				if name == msg.Id {
					id_slice[i] = id_slice[len(id_slice)-1]
					id_slice = id_slice[:len(id_slice)-1]
					break
				}
			}
			mu_id.Unlock()
			return
		}
		//fmt.Printf("got message %v from %s\n", msg, store_id)
		if msg.Msg_type == 1 || msg.Msg_type == 4 { //priority requests
			mu_p.Lock()
			msg.Priority = max_priority_seen + 1
			max_priority_seen++
			mu_p.Unlock()
			msg.Msg_type++ //correct msg type to response
			t := time.Now()
			item := &Item{msg: *msg, valid: false, start: t}

			p := Pair{Process: msg.Process_num, Id: msg.Id}
			//add message/item pair to map so we can update it later
			mu_msg_map.Lock()
			msg_map[p] = item
			mu_msg_map.Unlock()
			mu_pq.Lock()
			heap.Push(&pq, item) //add message to pq as undeliverable
			mu_pq.Unlock()

			send_request(msg.Id, *msg)
		} else if msg.Msg_type == 2 || msg.Msg_type == 5 { //priority responses
			//keep track of highest priority and increment response counter
			i := msg.Process_num - 1
			mu_msg.Lock() //update priority if it's higher
			if i >= len(msg_slice) {
				fmt.Printf("error accessing with priority %d\n", i)
				fmt.Println(msg_slice)
				fmt.Println(msg)
				fmt.Println(cur_process)
				os.Exit(1)
			}
			if msg.Priority > msg_slice[i].Priority {
				msg_slice[i].Priority = msg.Priority
			}
			mu_msg.Unlock()

			mu_proc.Lock()
			proc_slice[i]++
			mu_proc.Unlock()
		} else if msg.Msg_type == 3 || msg.Msg_type == 6 {
			pq.update(*msg, true, msg.Priority, msg.Msg_type) //update with final priority and valid

			mu_bal.Lock()
			mu_pq.Lock()
			t := time.Now().Unix()
			//check for messages from dead nodes and pop them
			for (pq.Len() > 0) && (pq[0].valid == false) {
				if t-pq[0].start.Unix() <= 4 {
					break //if nothing is dead
				}
				_ = heap.Pop(&pq).(*Item)
			}
			for (pq.Len() > 0) && (pq[0].valid == true) {
				item := heap.Pop(&pq).(*Item)

				b1 := bal_map[item.msg.Acct1]
				b2 := bal_map[item.msg.Acct2]
				//fmt.Printf("delivering msg %v\n", item.msg)
				//fmt.Printf("b1: %d, b2: %d\n", b1, b2)
				if item.msg.Msg_type == 3 {
					bal_map[item.msg.Acct1] = b1 + item.msg.Amt
				} else if b1-item.msg.Amt < 0 {
					fmt.Printf("INVALID TRANSACTION\n")
				} else {
					bal_map[item.msg.Acct1] = b1 - item.msg.Amt
					bal_map[item.msg.Acct2] = b2 + item.msg.Amt
				}
				//fmt.Println(bal_map)

				//write values to logs
				//dur1 := float64((time.Now().UnixNano() - item.msg.gen.UnixNano()) / 1000)
				//dur := dur1 / 1000000 //seconds
				dur := time.Now().UnixNano() - item.msg.Gen
				//fmt.Println(item.start)
				//fmt.Println(item.msg.gen)
				mu_w.Lock()
				_, err := fmt.Fprintf(W_b, "%v\n", bal_map)
				handle_err(err)
				//fmt.Printf("wrote %d bytes\n", d)

				_, err = fmt.Fprintf(W_t, "%s %d %d\n", item.msg.Id, item.msg.Process_num, dur)
				handle_err(err)
				//fmt.Printf("wrote %d bytes\n", d)
				W_b.Flush()
				W_t.Flush()
				mu_w.Unlock()
				//log_b.WriteString(bal_map)
				//fmt.Printf(" %d\n", pq.Len())
			}
			mu_bal.Unlock()
			mu_pq.Unlock()
		}
	}
}
func connect_node(port, addr, node_id string) {
	//fmt.Printf("addr is %s port is %s\n", addr, port)
	//fmt.Printf("Connecting to: %s\n", strings.Join([]string{addr, port}, ":"))
	i := 0
	msg := &Message{}
	for {
		// if i%100 == 0 {
		// 	fmt.Printf("%d ", i)
		// }
		conn_out, err := net.Dial("tcp", strings.Join([]string{addr, port}, ":"))
		if err == nil {
			//defer conn_out.Close()
			encoder := gob.NewEncoder(conn_out)
			msg.Id = my_id
			err := encoder.Encode(msg)
			handle_err(err)

			mu_out.Lock()
			out_map[node_id] = encoder
			mu_out.Unlock()
			//fmt.Printf("Connected to %s\n", addr)
			break
		}
		i++
	}
	//fmt.Println(out_map)
}

func main() {
	argv := os.Args[1:]
	if len(argv) != ARG_NUM_NODE {
		fmt.Fprintf(os.Stderr, "usage ./node <ID> <PORT> <CONFIG_FILEPATH>")
		os.Exit(1)
	}
	my_id = argv[0]
	node_port := ":" + argv[1]
	filepath := argv[2]
	f, err := os.Open("./" + filepath)
	handle_err(err)
	defer f.Close()

	//allocate maps
	in_map = make(map[string]net.Conn)
	out_map = make(map[string]*gob.Encoder)
	msg_map = make(map[Pair]*Item)
	bal_map = make(map[string]int)

	//Create PQ
	pq = make(PriorityQueue, 0)
	heap.Init(&pq)

	//initialize log file for times and balances
	log_b, err := os.Create("balance_log.txt")
	handle_err(err)
	log_t, err := os.Create("time_log.txt")
	handle_err(err)
	log_b.Chmod(0777)
	log_t.Chmod(0777)
	defer log_b.Close()
	defer log_t.Close()
	W_b = bufio.NewWriter(log_b)
	W_t = bufio.NewWriter(log_t)

	//add self to group
	id_slice = append(id_slice, my_id)
	addr_slice = append(addr_slice, "localhost")
	port_slice = append(port_slice, node_port[1:])     //remove front colon
	go connect_node(node_port[1:], "localhost", my_id) //connect to self

	//add others to group and send connection request
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		//fmt.Printf("got: %s\n", scanner.Text())
		hello := strings.Split(scanner.Text(), " ")
		if len(hello) == 3 {
			id_slice = append(id_slice, hello[0])
			addr_slice = append(addr_slice, hello[1])
			port_slice = append(port_slice, hello[2])
			go connect_node(hello[2], hello[1], hello[0])
		} else if len(hello) == 1 && len(scanner.Text()) != 0 {
			//my_id = scanner.Text()
			//fmt.Printf("there are %s nodes to connect to\n", scanner.Text())
			//fmt.Printf("got %v %d \n", scanner.Text(), len(scanner.Text()))
		}
	}

	//display name and nodes to connect to
	//fmt.Printf("My name is %s\n", my_id)
	//fmt.Printf("%v \n%v \n%v \n", id_slice, port_slice, addr_slice)
	connected_nodes = len(id_slice)

	//listen for incoming connections
	ln, err := net.Listen("tcp", node_port)
	handle_err(err)
	defer ln.Close()

	//wait for all nodes to be connected
	i := 0
	for i < len(id_slice) {
		conn_in, err := ln.Accept()
		handle_err(err)
		go handle_receive(conn_in)
		i++
	}
	//wait for all incoming/outgoing connections to be established
	//use waitgroup???
	for len(in_map) < len(id_slice) || len(out_map) < len(id_slice) {
	}
	fmt.Printf("lets get started\n")

	//start reading stdin
	reader := bufio.NewReader(os.Stdin)
	for {
		text, err := reader.ReadString('\n')
		handle_err(err)
		hello := strings.Split(text, " ")
		//trim trailing newline
		hello[len(hello)-1] = strings.TrimRight(hello[len(hello)-1], "\n")
		if len(hello) == 3 && hello[0] == "DEPOSIT" {
			//fmt.Printf("got %v\n", hello)
			prepare_request(hello)
		} else if len(hello) == 5 && hello[0] == "TRANSFER" {
			//fmt.Printf("got %v\n", hello)
			prepare_request(hello)
		} else {
			//fmt.Printf("invalid input\n")
		}
	}
}

// type Message struct {
// 	id           string
// 	msg_type,amt int
// 	acct1, acct2 string
// 	priority     int
//  process_num  int
// }
//0 first msg, 1 deposit request, 2 deposit response, 3 final deposit
//4 trans request, 5 trans response, 6 final trans
//todo use waitgroups here to keep track of failed nodes
func prepare_request(trans []string) {
	size := len(trans) //num of words in stdin msg

	var msg Message //put the intended message in here
	mu_p.Lock()
	p := max_priority_seen + 1
	mu_p.Unlock()

	proc_slice = append(proc_slice, 0)
	//t := time.Now().Unix()

	if size == 3 {
		cash, err := strconv.Atoi(trans[2])
		handle_err(err)
		msg = Message{Id: my_id, Msg_type: 1, Acct1: trans[1], Gen: time.Now().UnixNano(),
			Acct2: "", Priority: p, Amt: cash, Process_num: cur_process}
		mu_id.Lock()
		for _, node := range id_slice {
			go send_request(node, msg)
		}
		mu_id.Unlock()
	}
	if size == 5 {
		cash, err := strconv.Atoi(trans[4])
		handle_err(err)
		msg = Message{Id: my_id, Msg_type: 4, Acct1: trans[1], Gen: time.Now().UnixNano(),
			Acct2: trans[3], Priority: p, Amt: cash, Process_num: cur_process}
		mu_id.Lock()
		for _, node := range id_slice { //golang is fucking weird
			go send_request(node, msg)
		}
		mu_id.Unlock()
	}
	//wg_out.Wait() //wait for all nodes to be sent
	mu_msg.Lock()
	msg_slice = append(msg_slice, msg)
	mu_msg.Unlock()
	go receive_priorities(cur_process, msg)
	cur_process++ //increment current process
}

//gob and send
func send_request(node string, msg Message) {
	//defer wg_in.Done()
	// fmt.Println(msg)
	// fmt.Println(out_map)
	// fmt.Printf("sending to %s\n", node)
	// fmt.Printf("my id is %s\n", my_id)
	encoder := out_map[node] //get the connection
	if encoder == nil {
		fmt.Printf("error trying to get %v\n", node)
		fmt.Println(out_map)
		fmt.Println(in_map)
		fmt.Println(id_slice)
		os.Exit(1)
	}
	//fmt.Printf("send msg %v to %s\n", msg, node)
	encoder.Encode(msg) //send encoded message to corresponding node
	//fmt.Printf("sent msg %v to %s\n", msg, node)
}

/*
Wait for all processes to send a requested priority
Once recieved, choose priority and send final priority
*/
func receive_priorities(cur_p int, msg Message) {
	//need to eventually keep track of failed nodes
	for proc_slice[cur_p-1] != len(id_slice) {
	}
	//fmt.Printf("sending final priorities\n")
	// if cur_p > 10 {
	// 	fmt.Printf("sayanara\n")
	// 	os.Exit(1)
	// }
	i := msg.Process_num - 1
	p := msg_slice[i].Priority
	msg.Priority = p
	msg.Msg_type += 2 //mark as final priority message
	mu_id.Lock()
	for _, node := range id_slice {
		go send_request(node, msg) //may need to make this non-threaded. We'll see
	}
	mu_id.Unlock()
	mu_p.Lock()
	max_priority_seen = p
	mu_p.Unlock()
}
