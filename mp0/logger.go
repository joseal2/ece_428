package main

import (
	"fmt"
	"os"
	"net"
	"bufio"
	"log"
)

const (
	connHost = "localhost"
	connPort = "8080"
	connType = "tcp"
)

func main() {
	fmt.Println("Logger starting")
	args := os.Args[1:]
	if len(args) > 1{
		fmt.Println("too many arguments")
		os.Exit(1)
	}
	port := args[0]

	fmt.Println("Starting " + connType + " server on " + connHost + ":" + port)
	l, err := net.Listen(connType, connHost+":"+port)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	defer l.Close()

	for {
		c, err := l.Accept()
		if err != nil {
			fmt.Println("Error listening:", err.Error())
			os.Exit(1)
		}
		fmt.Println("Client connected.")

		fmt.Println("Client " + c.RemoteAddr().String() + " connected.")

		go handleConnection(c)
	
	}

}

func handleConnection(conn net.Conn){
	buffer, err := bufio.NewReader(conn).ReadBytes('\n')

	if err != nil {
		fmt.Println("Client left.")
		conn.Close()
		return
	}
	log.Println("Client message: ", string(buffer[:len(buffer)-1]))
	conn.Write(buffer)
	handleConnection(conn)
}
